export interface mapConfig {
    mapModel: mapModel,
    height?: string,
    width?: string,
    zoom: string
}

export interface mapModel {
    name: string,
    apiKey: string
}
