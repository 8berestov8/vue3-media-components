export interface mapItem {
    id: number | string;
    title: string;
    subtitle?: string;
    description?: string;
    thumbnailSrc: string;
    coordinates: mapCoordinates;
}

export interface mapCoordinates {
    lat?: string | number;
    long?: string | number;
    city?: string;
}

export interface markerClickedInterface {
    item: mapItem,
    posX: number,
    posY: number
}
