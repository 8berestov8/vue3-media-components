export default class MapItem {
    id;
    thumbnailSrc;
    title;
    subtitle = '';
    description = '';
    coordinates = {};
    constructor(id, title, thumbnailSrc, coordinates) {
        this.id = id;
        this.thumbnailSrc = thumbnailSrc;
        this.title = title;
        this.coordinates = coordinates;
    }
}
//# sourceMappingURL=mapItem.js.map